var express = require('express'),
  app = express(),
  port = process.env.PORT || 9999;

var routes = require('./route/router'); //importing route
routes(app);

app.listen(port);

console.log('Server started on port ' + port);