'use strict';
module.exports = function(app) {
  var homeController = require('../api/homeController');

  app.route('/')
    .get(homeController.grettings);
};