# Node JS Hello World Example

## Overview
Node JS projects have similar hierarchy with Spring Boot applications, but with almost instant start-up and shorter syntax.
The project demonstrated how to implement a simple GET HTTP service using Node JS.

## Structure
- package.json: equals to `pom.xml`
- server.js: euqals to Application.java (the entry point of the application)
- api[folder]: a list of controllers describing how to handle different HTTP methods
- route[folder]: a list of routers describing available end points with corresponding HTTP methods
- package-lock.json: auto-generated
- node_modules: euqals to `.m2/repository`

## End Point
The end point of the project is http://localhost:9999/?name={your_name}.

## Start Up The Application
To start up the application, `cd` to the root of the project and simply enter `npm start`, you will then see
```
[nodemon] 1.19.1
[nodemon] to restart at any time, enter `rs`
[nodemon] watching: *.*
[nodemon] starting `node server.js`
Server started on port 9999
```
if the application starts successfully.

Hit the service now and give a try!

Note: you can modify the code and press `save` at any time and the server will restart immediately. Alternatively, type `rs` in your console.